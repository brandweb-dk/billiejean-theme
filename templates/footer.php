<footer>
  <div class="content-info">
    <div class="container">
      <div class="row">
        <div class="col-md-3">
          <?php dynamic_sidebar('footer-1'); ?>
        </div>

        <div class="col-md-2 col-md-offset-1">
          <?php dynamic_sidebar('footer-2'); ?>
        </div>
      </div>
    </div>
  </div>

  <div class="copyright">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <p>
            Alle rettigheder forbeholdes © 2016 Billie Jean Bar
          </p>
        </div>
      </div>
    </div>
  </div>
</footer>
